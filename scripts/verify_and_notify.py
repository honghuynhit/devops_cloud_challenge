import json
import boto3
from botocore.exceptions import ClientError
from botocore.config import Config
import hcl2


LOG_FILENAME = 'consistency.txt'


def read_tfstate():
    terraform_tfstate_json_filename = 'terraform.tfstate.json'
    json_file = open(terraform_tfstate_json_filename, 'r')
    terraform_tfstate_json = json.load(json_file)
    json_file.close()
    # log(terraform_tfstate_json)
    return terraform_tfstate_json


def extract_vpc_and_region(json):
    aws_region = ''
    vpc_id = ''
    for item in json['resources']:
        if item["type"] == "aws_vpc":
            aws_region = item['instances'][0]['attributes']['arn'].split(':')[3]
            vpc_id = item['instances'][0]['attributes']['id']
    # log(aws_region)
    # log(vpc_id)
    return aws_region, vpc_id


def get_aws_security_group(aws_region, vpc_id, security_group_name):
    my_config = Config(
        region_name = aws_region,
        signature_version = 'v4',
        retries = {
            'max_attempts': 10,
            'mode': 'standard'
        }
    )

    ec2 = boto3.client('ec2', config=my_config)

    try:
        response = ec2.describe_security_groups(Filters=[{'Name': 'vpc-id', 'Values': [vpc_id]},
                                                         {'Name': 'group-name', 'Values': [security_group_name]}])
    except ClientError as e:
        # log(e)
        return e
    # log(response)
    return response


def extract_aws_security_group(aws_region, vpc_id, security_group_name):
    json = {'security_group_name': security_group_name}
    aws_sec_grp = get_aws_security_group(aws_region, vpc_id, security_group_name)
    for item in aws_sec_grp['SecurityGroups']:
        if item['GroupName'] == security_group_name:
            json['ingress'] = item['IpPermissions']
            json['egress'] = item['IpPermissionsEgress']
    # log(json)
    return json


def extract_tfstate_security_group(tfstate, security_group_name):
    rules = {'security_group_name': security_group_name}
    for item_r in tfstate['resources']:
        if item_r['type'] == 'aws_security_group' and item_r['name'] == security_group_name:
            for item_i in item_r['instances']:
                rules['egress'] = item_i['attributes']['egress']
                rules['ingress'] = item_i['attributes']['ingress']
    return rules


def extract_tf_security_group(tf_file_json, security_group_name):
    rules = {'security_group_name': security_group_name}
    for item_r in tf_file_json['resource']:
        if 'aws_security_group' in item_r:
            if security_group_name in item_r['aws_security_group']:
                if 'ingress' in item_r['aws_security_group'][security_group_name]:
                    rules['ingress'] = item_r['aws_security_group'][security_group_name]['ingress']
                else:
                    rules['ingress'] = []
                if 'egress' in item_r['aws_security_group'][security_group_name]:
                    rules['egress'] = item_r['aws_security_group'][security_group_name]['egress']
                else:
                    rules['egress'] = []
    return rules


def log(text):
    print(text)
    with open(LOG_FILENAME, 'a') as log_file:
        log_file.write(text + "\n")


def read_tf_file():
    with open('sec_rules_custom.tf', 'r') as f:
        terraform_dict = hcl2.load(f)
    return terraform_dict


def main():
    log('\nExtracting data from Terraform tfsates file')
    tfstate_json = read_tfstate()
    tfstate_custom_sg_private_security_group = extract_tfstate_security_group(tfstate_json, 'custom_sg_private')
    log('tfstate security group: {}\n{}'.format('custom_sg_private', tfstate_custom_sg_private_security_group))
    tfstate_custom_sg_public_security_group = extract_tfstate_security_group(tfstate_json, 'custom_sg_public')
    log('tfstate security group: {}\n{}'.format('custom_sg_public', tfstate_custom_sg_public_security_group))
    aws_region, vpc_id = extract_vpc_and_region(tfstate_json)
    log('AWS Region: {}\nVPC ID:{}'.format(aws_region, vpc_id))

    log('\nExtracting data from repo tf files')
    tf_file_json = read_tf_file()
    tf_custom_sg_private_security_group = extract_tf_security_group(tf_file_json, 'custom_sg_private')
    log('tf file security group: {}\n{}'.format('custom_sg_private',tf_custom_sg_private_security_group))
    tf_custom_sg_public_security_group = extract_tf_security_group(tf_file_json, 'custom_sg_public')
    log('tf file security group: {}\n{}'.format('custom_sg_public', tf_custom_sg_public_security_group))

    log('\nGetting data from AWS')
    aws_custom_sg_private_security_group = extract_aws_security_group(aws_region, vpc_id, 'custom_sg_private')
    log('AWS security group: {}\n{}'.format('custom_sg_private', aws_custom_sg_private_security_group))
    aws_custom_sg_public_security_group = extract_aws_security_group(aws_region, vpc_id, 'custom_sg_public')
    log('AWS security group: {}\n{}'.format('custom_sg_public', aws_custom_sg_public_security_group))


if __name__ == '__main__':
    main()