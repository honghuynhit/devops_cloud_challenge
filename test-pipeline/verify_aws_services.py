import json
import boto3
from botocore.exceptions import ClientError
from botocore.config import Config
import requests
import sys

def read_tfstate():
    terraform_tfstate_json_filename = 'terraform.tfstate.json'
    json_file = open(terraform_tfstate_json_filename, 'r')
    terraform_tfstate_json = json.load(json_file)
    json_file.close()
    # print(terraform_tfstate_json)
    return terraform_tfstate_json


def extract_vpc_and_region(json):
    aws_region = ''
    vpc_id = ''
    for item in json['resources']:
        if item["type"] == "aws_vpc":
            aws_region = item['instances'][0]['attributes']['arn'].split(':')[3]
            vpc_id = item['instances'][0]['attributes']['id']
    # print(aws_region)
    # print(vpc_id)
    return aws_region, vpc_id


def get_aws_security_group(aws_region, vpc_id, security_group_name):
    my_config = Config(
        region_name = aws_region,
        signature_version = 'v4',
        retries = {
            'max_attempts': 10,
            'mode': 'standard'
        }
    )

    ec2 = boto3.client('ec2',
                       config=my_config)
    try:
        response = ec2.describe_security_groups(Filters=[{'Name': 'vpc-id', 'Values': [vpc_id]},
                                                         {'Name': 'group-name', 'Values': [security_group_name]}])
    except ClientError as e:
        print(e)
        return e
    # print(response)
    return response


def extract_aws_security_group_id(aws_region, vpc_id, security_group_name):
    aws_sec_grp = get_aws_security_group(aws_region, vpc_id, security_group_name)
    return aws_sec_grp['SecurityGroups'][0]['GroupId']


def get_aws_security_group_members(aws_region, vpc_id, security_group_id):
    my_config = Config(
        region_name = aws_region,
        signature_version = 'v4',
        retries = {
            'max_attempts': 10,
            'mode': 'standard'
        }
    )

    ec2 = boto3.client('ec2', config=my_config)

    try:
        response = ec2.describe_network_interfaces(Filters=[{'Name': 'vpc-id', 'Values': [vpc_id]},
                                                         {'Name': 'group-id', 'Values': [security_group_id]}])
    except ClientError as e:
        # print(e)
        return e
    # print(response)
    return response


def extract_aws_security_group_members(aws_region, vpc_id, security_group_id):
    aws_sec_grp_members = get_aws_security_group_members(aws_region, vpc_id, security_group_id)
    ip_members = []
    for item in aws_sec_grp_members['NetworkInterfaces']:
        ip_members.append(item['Association']['PublicIp'])
    # print(json)
    return ip_members


def get_member_status(ip):
    url = 'http://' + ip
    try:
        requests.get(url, timeout=5)
    except:
        return('Failed')
    else:
        return('Ok')


def main():
    print('\nExtracting data from Terraform tfsates file')
    tfstate_json = read_tfstate()
    aws_region, vpc_id = extract_vpc_and_region(tfstate_json)

    print('AWS Region: {}\nVPC ID:{}'.format(aws_region,vpc_id))

    print('\nGetting data from AWS')
    aws_custom_sg_public_security_group_id = extract_aws_security_group_id(aws_region, vpc_id, 'custom_sg_public')
    print('AWS security group: {}\nID: {}'.format('custom_sg_public', aws_custom_sg_public_security_group_id))
    aws_custom_sg_public_security_group_members = extract_aws_security_group_members(aws_region, vpc_id, aws_custom_sg_public_security_group_id)
    print('IP members: ' + str(aws_custom_sg_public_security_group_members))
    
    print('\nChecking IP members port 80')

    file = open('services_status.txt', 'w')
    status_ok = True
    for member in aws_custom_sg_public_security_group_members:
        status = get_member_status(member)
        if status == 'Failed':
            status_ok = False
        print('Member: {} - Status: {}'.format(member, status))
        file.write('Member: {} - Status: {}'.format(member, status))
    file.close()

    if status_ok:
        sys.exit(0)
    else:
        sys.exit(1)

if __name__ == '__main__':
    main()