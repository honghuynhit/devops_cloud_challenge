resource "aws_security_group" "infra_sg_public" {
  name        = "infra_public"
  description = "Infrastucture Services"
  vpc_id      = aws_vpc.ipspace.id
  tags = {
    Training = "https://www.ipspace.net/PubCloud/"
  }
}

/*
To avoid circular dependency with aws_security_group.infra_private.id since it
is not created yet, these security rules has to be an independent resource. In this
way the creation will be delayed until that dependant resource is created
*/
resource "aws_security_group_rule" "front_to_back_ssh_egress" {
    type = "egress"
    description = "SSH OUT"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_group_id = aws_security_group.infra_sg_public.id
    /*
    weird thing, since it is an egress rule, the following line should be destination_security_group_id,
    but is no, is not supported, instead you have to use source like if it was an ingress rule.
    */
    source_security_group_id = aws_security_group.infra_sg_private.id
}

// SSH from internet to front/public Instance
resource "aws_security_group_rule" "pub_front_in_ssh" {
    type = "ingress"
    description = "SSH_IN"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_group_id = aws_security_group.infra_sg_public.id
    cidr_blocks = ["0.0.0.0/0"]
}

// Basic ICMP for testing
resource "aws_security_group_rule" "pub_front_in_icmp" {
    type = "ingress"
    description = "ICMP_IN"
    from_port = -1
    to_port = -1
    protocol = "icmp"
    security_group_id = aws_security_group.infra_sg_public.id
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "pub_front_out_icmp" {
    type = "egress"
    description = "ICMP_OUT"
    from_port = -1
    to_port = -1
    protocol = "icmp"
    security_group_id = aws_security_group.infra_sg_public.id
    cidr_blocks = ["0.0.0.0/0"]
  }

// HTTP egress for Intances software upgrade and install
resource "aws_security_group_rule" "front_to_pub_https_egress" {
    type = "egress"
    description = "HTTPS_OUT"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    security_group_id = aws_security_group.infra_sg_public.id
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "front_to_pub_http_egress" {
    type = "egress"
    description = "HTTP_OUT"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_group_id = aws_security_group.infra_sg_public.id
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group" "infra_sg_private" {
  name        = "infra_private"
  description = "Infrastucture Services"
  vpc_id      = aws_vpc.ipspace.id

  ingress {
    description = "SSH IN"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [aws_security_group.infra_sg_public.id]
  }

  egress {
    description = "SSH OUT"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [aws_security_group.infra_sg_public.id]
  }

  ingress {
    description = "Ping IN"
    from_port = -1
    to_port = -1
    protocol = "icmp"
    security_groups = [aws_security_group.infra_sg_public.id]
  }

  egress {
    description = "Ping OUT"
    from_port = -1
    to_port = -1
    protocol = "icmp"
    security_groups = [aws_security_group.infra_sg_public.id]
  }

  tags = {
    Training = "https://www.ipspace.net/PubCloud/"
  }
}
