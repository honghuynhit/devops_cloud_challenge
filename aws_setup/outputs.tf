/*
Print outputs to screen for debug purposes
*/
output "vpc_name" {
  value = aws_vpc.ipspace.tags.Name
  description = "The Name of the VPC."
}

output "vpc_id" {
  value = aws_vpc.ipspace.id
  description = "The ID of the VPC."
}

output "front_instance_name" {
  value = aws_instance.front.tags.Name
  description = "The Name of the front/public instance."
}

output "front_instance_id" {
  value = aws_instance.front.id
  description = "The ID of the front/public instance."
}

output "front_private_ip" {
  value = aws_instance.front.private_ip
  description = "The private IP address of Public server instance."
}

output "front_public_ip" {
  value = aws_instance.front.public_ip
  description = "The Public IP address of Public server instance."
}

output "back_instance_name" {
  value = aws_instance.back.tags.Name
  description = "The Name of the Private instance."
}

output "back_instance_id" {
  value = aws_instance.back.id
  description = "The ID of the Private instance."
}

output "back_private_ip" {
  value = aws_instance.back.private_ip
  description = "The private IP address of Public server instance."
}

output "infra_sg_public_name" {
  value = aws_security_group.infra_sg_public.name
  description = "The Name fo the public SG for basic infrastructure."
}

output "infra_sg_public_id" {
  value = aws_security_group.infra_sg_public.id
  description = "The ID fo the public SG for basic infrastructure."
}

output "infra_sg_private_name" {
  value = aws_security_group.infra_sg_private.name
  description = "The Name fo the private SG for basic infrastructure."
}

output "infra_sg_private_id" {
  value = aws_security_group.infra_sg_private.id
  description = "The ID fo the private SG for basic infrastructure."
}

output "custom_sg_public_name" {
  value = aws_security_group.custom_sg_public.name
  description = "The Name fo the public SG for basic infrastructure."
}

output "custom_sg_public_id" {
  value = aws_security_group.custom_sg_public.id
  description = "The ID fo the public SG for basic infrastructure."
}

output "custom_sg_private_name" {
  value = aws_security_group.custom_sg_private.name
  description = "The Name fo the private SG for basic infrastructure."
}

output "custom_sg_private_id" {
  value = aws_security_group.custom_sg_private.id
  description = "The ID fo the private SG for basic infrastructure."
}
