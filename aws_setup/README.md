Full project description at:

https://adriangiacometti.net/index.php/2021/04/08/cloud-network-automation-challenge-part-1-3/

https://adriangiacometti.net/index.php/2021/04/08/cloud-network-automation-challenge-part-2-3/

https://adriangiacometti.net/index.php/2021/04/08/cloud-network-automation-challenge-part-3-3/


There are 2 ways to use GitLab managed Terraform state.

1. launching Terraform from the local machine and storing in GitLab with the http backend
2. use GitLab CI (pipelines)

This setup directory will use option 1 to setup the LAB, while the root
folder will use method 2, to read the Terraform states created at the setup
time, and modify security rules within the pipeline.

Below is an abstract on how to setup the environment.

Read the link below for more detalis on how to initialize and use each one.

https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html


#### Option 1:
Use Terraform from local machine to setup the lab.

**1.- Setup the Terraform backend in your .tf file with**
```
terraform {
  backend "http" {
  }
}
```

**2.- Generate your personal GitLab access token**

Click you avatar upper-right -> preferences -> access tokens -> name, 
expiration date and `api` scope.


**3.- Initialize Terraform**

Get your GitLab project ID and Name from settings -> general

Replace with your information in the following script.

For `YOUR-STATE-NAME` you can use your project name, but it could be another custom name. The script use the project
name, if you change it, change acordingly at the end of the variable TF_ADDRESS of the pipeline .gitlab-ci.yml.

```
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/<YOUR-PROJECT-ID>/terraform/state/<YOUR-STATE-NAME>" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/<YOUR-PROJECT-ID>/terraform/state/<YOUR-STATE-NAME>/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/<YOUR-PROJECT-ID>/terraform/state/<YOUR-STATE-NAME>/lock" \
    -backend-config="username=<YOUR-USERNAME>" \
    -backend-config="password=<YOUR-ACCESS-TOKEN>" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

 **to update your GitLab API password you can manually edit `.terraform/terrafom.state` or 
 re init terraform with the above cmd*

**4.- View GitLab managed Terraform States**

In your GitLab project go to Operations -> Terraform
