// The following resources will the ones managed by the pipeline

resource "aws_security_group" "custom_sg_public" {
  name = "custom_sg_public"
  description = "Instances Services"
  vpc_id = data.terraform_remote_state.cloud_networking.outputs.vpc_id
  tags = {
    Training = "https://www.ipspace.net/PubCloud/"
  }
  ingress {
    description = "HTTP_IN"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "custom_sg_private" {
  name = "custom_sg_private"
  description = "Instances Services"
  vpc_id = data.terraform_remote_state.cloud_networking.outputs.vpc_id
  tags = {
    Training = "https://www.ipspace.net/PubCloud/"
  }
  ingress {
    description = "MYSQL_IN"
    from_port = 3000
    to_port = 3000
    protocol = "tcp"
    security_groups = [aws_security_group.custom_sg_public.id]
  }
}